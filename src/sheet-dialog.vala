internal class DialogOverlay : Gtk.Widget {
    private const int HORZ_MARGIN = 64;
    private const int VERT_MARGIN = 64;

    public signal void hidden ();

    private Adw.SpringAnimation animation;
    private Gtk.Widget dimming;
    private Adw.Bin dialog_bin;

    private Gtk.Widget? _dialog;
    public Gtk.Widget? dialog {
        get { return _dialog; }
        set {
            if (dialog == value)
                return;

            _dialog = value;

            dialog_bin.child = dialog;
        }
    }

    private bool _show_dialog;
    public bool show_dialog {
        get { return _show_dialog; }
        set {
            if (show_dialog == value)
                return;

            _show_dialog = value;

            animation.value_from = animation.value;
            animation.value_to = show_dialog ? 1 : 0;
            animation.play ();
        }
    }

    private int _preferred_width;
    public int preferred_width {
        get { return _preferred_width; }
        set {
            if (preferred_width == value)
                return;

            _preferred_width = value;

            if (animation.value > 0)
                queue_allocate ();
        }
    }

    private int _preferred_height;
    public int preferred_height {
        get { return _preferred_height; }
        set {
            if (preferred_height == value)
                return;

            _preferred_height = value;

            if (animation.value > 0)
                queue_allocate ();
        }
    }

    construct {
        dimming = new Gtk.WindowHandle ();
        dimming.opacity = 0;
        dimming.set_child_visible (false);
        dimming.add_css_class ("dimming");
        dimming.set_parent (this);

        dialog_bin = new Adw.Bin ();
        dialog_bin.set_child_visible (false);
        dialog_bin.add_css_class ("dialog");
        dialog_bin.set_parent (this);

        animation = new Adw.SpringAnimation (
            this, 0, 1, new Adw.SpringParams (1, 1, 400),
            new Adw.CallbackAnimationTarget (value => {
                dimming.opacity = value.clamp (0, 1);
                dimming.set_child_visible (value > 0);
                dialog_bin.opacity = value.clamp (0, 1);
                dialog_bin.set_child_visible (value > 0);
                queue_allocate ();
            })
        );
        animation.done.connect (() => {
            queue_allocate ();

            if (animation.value < 0.5) {
                dimming.set_child_visible (false);
                dialog_bin.set_child_visible (false);

                hidden ();
            }
        });
        animation.epsilon = 0.0001;
    }

    protected override void dispose () {
        if (dimming != null) {
            dimming.unparent ();
            dimming = null;
        }

        if (dialog_bin != null) {
            dialog_bin.unparent ();
            dialog_bin = null;
        }

        base.dispose ();
    }

    protected override bool contains (double x, double y) {
        return false;
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int min,
        out int nat,
        out int min_baseline,
        out int nat_baseline)
    {
        int dialog_min, dialog_nat;
        int dimming_min, dimming_nat;
        min = nat = 0;

        dialog_bin.measure (orientation, for_size, out dialog_min, out dialog_nat, null, null);
        dimming.measure (orientation, for_size, out dimming_min, out dimming_nat, null, null);

        if (orientation == HORIZONTAL) {
            min = int.max (dimming_min, dialog_min + HORZ_MARGIN * 2);
            nat = int.max (dimming_nat, dialog_nat + HORZ_MARGIN * 2);
        } else {
            min = int.max (dimming_min, dialog_min + VERT_MARGIN * 2);
            nat = int.max (dimming_nat, dialog_nat + VERT_MARGIN * 2);
        }
        min_baseline = nat_baseline = -1;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (!dialog_bin.get_child_visible ())
            return;

        dimming.allocate (width, height, baseline, null);

        int dialog_height, min_dialog_height;
        int dialog_width, min_dialog_width;

        if (preferred_height < 0) {
            dialog_bin.measure (VERTICAL, -1, out min_dialog_height, out dialog_height, null, null);
        } else {
            dialog_bin.measure (VERTICAL, -1, out min_dialog_height, null, null, null);
            dialog_height = preferred_height;
        }

        if (preferred_width < 0) {
            dialog_bin.measure (HORIZONTAL, -1, out min_dialog_width, out dialog_width, null, null);
        } else {
            dialog_bin.measure (HORIZONTAL, -1, out min_dialog_width, null, null, null);
            dialog_width = preferred_width;
        }

        // TODO do wfh/hfw dance

        dialog_width = int.max (dialog_width, min_dialog_width);
        dialog_width = int.min (dialog_width, width - HORZ_MARGIN * 2);

        dialog_height = int.max (dialog_height, min_dialog_height);
        dialog_height = int.min (dialog_height, height - VERT_MARGIN * 2);

        float scale = 0.85f + 0.15f * (float) animation.value;

        var t = new Gsk.Transform ();
        t = t.translate ({ (width - dialog_width) / 2, (height - dialog_height) / 2 });
        t = t.translate ({ dialog_width / 2, dialog_height / 2 });
        t = t.scale (scale, scale);
        t = t.translate ({ -dialog_width / 2, -dialog_height / 2 });

        dialog_bin.allocate (dialog_width, dialog_height, baseline, t);
    }
}

public class SheetToplevel : Adw.Window {
    private Gtk.Widget _content;
    public Gtk.Widget? content2 {
        get { return _content; }
        set {
            _content = value;

            if (overlay != null)
                overlay.child = _content;
        }
    }

    private Gtk.Overlay overlay;

    private List<SheetDialog> dialogs;

    construct {
        overlay = new Gtk.Overlay ();

        if (content2 != null)
            overlay.child = content2;

        content = overlay;

        dialogs = new List<SheetDialog>();
    }

    protected override bool close_request () {
        if (dialogs.length () == 0)
            return Gdk.EVENT_PROPAGATE;

        var dialog = dialogs.last ().data;

        dialog.hidden.connect (() => {
            overlay.remove_overlay (dialog.get_widget ());
            dialogs.remove (dialog);
        });

        dialog.close ();

        return Gdk.EVENT_STOP;
    }

    public void present_dialog (SheetDialog dialog) {
        dialogs.append (dialog);
        overlay.add_overlay (dialog.get_widget ());
    }
}

public class SheetDialog : Object {
    public signal void hidden ();

    private Gtk.Widget _child;
    public Gtk.Widget? child {
        get { return _child; }
        set {
            if (child == value)
                return;

            _child = value;

            if (sheet != null)
                sheet.sheet = child;
            if (dialog != null)
                dialog.dialog = child;
        }
    }

    private int _preferred_width;
    public int preferred_width {
        get { return _preferred_width; }
        set {
            _preferred_width = value;

            if (dialog != null)
                dialog.preferred_width = preferred_width;
        }
    }

    private int _preferred_height;
    public int preferred_height {
        get { return _preferred_height; }
        set {
            _preferred_height = value;

            if (sheet != null)
                sheet.preferred_sheet_height = preferred_height;
            if (dialog != null)
                dialog.preferred_height = preferred_height;
        }
    }

    private Adw.BreakpointBin widget;
    private bool narrow;

    private Sheet? sheet;
    private DialogOverlay? dialog;
    private bool shown;

    construct {
        widget = new Adw.BreakpointBin () {
            width_request = 1,
            height_request = 1,
            child = new Adw.Bin (),
        };

        var condition2 = Adw.BreakpointCondition.parse ("max-width: 200px or min-width: 100px");
        var breakpoint2 = new Adw.Breakpoint (condition2);
        widget.add_breakpoint (breakpoint2);

        var condition = Adw.BreakpointCondition.parse ("max-width: 600px");
        var breakpoint = new Adw.Breakpoint (condition);
        widget.add_breakpoint (breakpoint);

        widget.notify["current-breakpoint"].connect (() => {
            narrow = widget.current_breakpoint == breakpoint;
            rebuild_ui ();
            shown = true;
        });
    }

    internal Gtk.Widget get_widget () {
        return widget;
    }

    private void rebuild_ui () {
        if (child != null)
            child.ref ();

        if (sheet != null) {
            sheet.sheet = null;
            sheet = null;
        }

        if (dialog != null) {
            dialog.dialog = null;
            dialog = null;
        }

        if (narrow) {
            sheet = new Sheet () {
                sheet = child,
                preferred_sheet_height = preferred_height,
            };

            if (shown)
                sheet.show_sheet = true;

            widget.child = sheet;

            if (!shown)
                sheet.show_sheet = true;
        } else {
            dialog = new DialogOverlay () {
                dialog = child,
                preferred_width = preferred_width,
                preferred_height = preferred_height,
            };

            if (shown)
                dialog.show_dialog = true;

            widget.child = dialog;

            if (!shown)
                dialog.show_dialog = true;
        }

        if (child != null)
            child.unref ();
    }

    public void close () {
        if (narrow) {
            sheet.hidden.connect (() => {
                hidden ();
            });

            sheet.show_sheet = false;
        } else {
            dialog.hidden.connect (() => {
                hidden ();
            });

            dialog.show_dialog = false;
        }
    }
}
