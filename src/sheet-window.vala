public class SheetWindow : Adw.Window {
    private unowned Gtk.Native? parent_window;
    private int parent_width;
    private int parent_height;

    private Adw.TimedAnimation delay_animation;
    private bool hidden;

    private bool first_map;
    private bool compact;
    private Sheet sheet;

    private int actual_default_width = -1;
    private int actual_default_height = -1;
    private bool block_notify;

    private Gtk.Widget? _content;
    public Gtk.Widget? content2 {
        get {return _content; }
        set {
            if (content2 == value)
                return;

            _content = value;

            if (compact)
                sheet.sheet = content2;
            else
                content = content2;
        }
    }

    public SheetWindow (Gtk.Window parent) {
        Object (
            transient_for: parent,
            modal: true,
            resizable: false,
            width_request: 1,
            height_request: 1
        );

        parent_width = -1;
        parent_height = -1;

//        maximize ();

        delay_animation = new Adw.TimedAnimation (
            this, 0, 1, 250, new Adw.CallbackAnimationTarget (value => {})
        );
        delay_animation.done.connect (() => {
            if (compact)
                sheet.show_sheet = true;
        });

        rebuild_ui ();

        notify["transient-for"].connect (parent_changed_cb);

        notify["default-width"].connect (() => {
            if (block_notify)
                return;

            actual_default_width = default_width;

            if (compact && default_width != -1) {
                block_notify = true;
                default_width = -1;
                block_notify = false;
            }
        });

        notify["default-height"].connect (() => {
            if (block_notify)
                return;

            actual_default_height = default_height;

            if (compact)
                sheet.preferred_sheet_height = actual_default_height;

            if (compact && default_height != -1) {
                block_notify = true;
                default_height = -1;
                block_notify = false;
            }
        });
    }

    private void update_compact () {
        bool compact = parent_width >= 0 && parent_width < 400;

        if (compact == this.compact)
            return;

        this.compact = compact;
        rebuild_ui ();
    }

    private void rebuild_ui () {
        if (_content != null)
            _content.ref ();

        if (sheet != null)
            sheet.sheet = null;

        content = null;

        if (compact) {
            block_notify = true;
            default_width = parent_width;
            default_height = parent_height;
            block_notify = false;

            add_css_class ("sheet");

            sheet = new Sheet ();
            sheet.preferred_sheet_height = actual_default_height;
            sheet.sheet = _content;
            sheet.add_css_class ("contents");
            sheet.hidden.connect (() => {
                hidden = true;
                close ();
            });

            if (!first_map)
                sheet.show_sheet = true;

            content = sheet;

            if (first_map)
                delay_animation.play ();
        } else {
            remove_css_class ("sheet");

            block_notify = true;
            default_width = actual_default_width;
            default_height = actual_default_height;
            block_notify = false;

            content = _content;

            delay_animation.reset ();
        }

        if (_content != null)
            _content.unref ();
    }

    private void parent_size_cb () {
        int w = parent_window.get_width ();
        int h = parent_window.get_height ();

        if (w == parent_width && h == parent_height)
            return;

        parent_width = w;
        parent_height = h;

        if (compact) {
            block_notify = true;
            default_width = parent_width;
            default_height = parent_height;
            block_notify = false;
        }

        update_compact ();

        first_map = false;
    }

    private void state_changed_cb () {
        Idle.add (() => {
            parent_size_cb ();
            return Source.REMOVE;
        });
    }

    private void parent_realize_cb () {
        assert (parent_window != null);

        var surface = parent_window.get_surface () as Gdk.Toplevel;

        surface.compute_size.connect (parent_size_cb);
        surface.notify["width"].connect (parent_size_cb);
        surface.notify["height"].connect (parent_size_cb);
        surface.notify["state"].connect (state_changed_cb);

        parent_size_cb ();
    }

    private void parent_unrealize_cb () {
        assert (parent_window != null);

        var surface = parent_window.get_surface () as Gdk.Toplevel;

        surface.compute_size.disconnect (parent_size_cb);
        surface.notify["width"].disconnect (parent_size_cb);
        surface.notify["height"].disconnect (parent_size_cb);
        surface.notify["state"].disconnect (state_changed_cb);

        parent_width = -1;
        parent_height = -1;

        update_compact ();
    }

    private void parent_window_notify_cb () {
        parent_window = null;
        parent_width = -1;
        parent_height = -1;

        update_compact ();
    }

    private void set_parent_window (Gtk.Native? parent) {
        if (parent_window == parent)
            return;

        if (parent_window != null) {
//            parent_window.realize.disconnect (parent_realize_cb);
//            parent_window.unrealize.disconnect (parent_unrealize_cb);

            if (parent_window.get_realized ())
                parent_unrealize_cb ();

            parent_window.weak_unref (parent_window_notify_cb);
        }

        parent_window = parent;

        if (parent_window != null) {
            parent_window.weak_ref (parent_window_notify_cb);

            if (parent_window.get_realized ())
                parent_realize_cb ();

//            parent_window.realize.connect (parent_realize_cb);
//            parent_window.unrealize.connect (parent_unrealize_cb);
        }
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int min,
        out int nat,
        out int min_baseline,
        out int nat_baseline)
    {
        base.measure (orientation, for_size, out min, out nat,
                      out min_baseline, out nat_baseline);

        min_baseline = nat_baseline = -1;

        if (!compact || true)
            return;

        if (orientation == HORIZONTAL)
            nat = int.max (nat, parent_width);
        else
            nat = int.max (nat, parent_height);
    }

    private void parent_changed_cb () {
        set_parent_window (transient_for);
    }

    protected override void map () {
        base.map ();

        first_map = true;

        parent_changed_cb ();
    }

    protected override void unmap () {
        set_parent_window (null);

        base.unmap ();
    }

    protected override bool close_request () {
        if (!compact)
            return Gdk.EVENT_PROPAGATE;

        if (hidden)
            return Gdk.EVENT_PROPAGATE;

        Idle.add (() => {
            hidden = false;
            sheet.show_sheet = false;

            return Source.REMOVE;
        });

        return Gdk.EVENT_STOP;
    }
}
