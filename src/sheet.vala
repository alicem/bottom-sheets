public class Sheet : Gtk.Widget {
    private const int TOP_MARGIN = 64;

    public signal void hidden ();

    private Adw.SpringAnimation animation;
    private Gtk.Widget dimming;
    private Adw.Bin sheet_bin;

    private Gtk.Widget? _sheet;
    public Gtk.Widget? sheet {
        get { return _sheet; }
        set {
            if (sheet == value)
                return;

            _sheet = value;

            sheet_bin.child = sheet;
        }
    }

    private bool _show_sheet;
    public bool show_sheet {
        get { return _show_sheet; }
        set {
            if (show_sheet == value)
                return;

            _show_sheet = value;

            animation.clamp = !show_sheet;
            animation.value_from = animation.value;
            animation.value_to = show_sheet ? 1 : 0;
            animation.play ();
        }
    }

    private int _preferred_sheet_height;
    public int preferred_sheet_height {
        get { return _preferred_sheet_height; }
        set {
            if (preferred_sheet_height == value)
                return;

            _preferred_sheet_height = value;

            if (animation.value > 0)
                queue_allocate ();
        }
    }

    construct {
        dimming = new Adw.Bin ();
        dimming.opacity = 0;
        dimming.set_child_visible (false);
        dimming.add_css_class ("dimming");
        dimming.set_parent (this);

        sheet_bin = new Adw.Bin ();
        sheet_bin.set_child_visible (false);
        sheet_bin.add_css_class ("sheet");
        sheet_bin.set_parent (this);

        animation = new Adw.SpringAnimation (
            this, 0, 1, new Adw.SpringParams (0.8, 1, 200),
            new Adw.CallbackAnimationTarget (value => {
                dimming.opacity = value.clamp (0, 1);
                dimming.set_child_visible (value > 0);
                sheet_bin.set_child_visible (value > 0);
                queue_allocate ();
            })
        );
        animation.done.connect (() => {
            queue_allocate ();

            if (animation.value < 0.5) {
                dimming.set_child_visible (false);
                sheet_bin.set_child_visible (false);

                hidden ();
            }
        });
        animation.epsilon = 0.0001;
    }

    protected override void dispose () {
        if (dimming != null) {
            dimming.unparent ();
            dimming = null;
        }

        if (sheet_bin != null) {
            sheet_bin.unparent ();
            sheet_bin = null;
        }

        base.dispose ();
    }

    protected override bool contains (double x, double y) {
        return false;
    }

    protected override void measure (
        Gtk.Orientation orientation,
        int for_size,
        out int min,
        out int nat,
        out int min_baseline,
        out int nat_baseline)
    {
        int sheet_min, sheet_nat;
        int dimming_min, dimming_nat;
        min = nat = 0;

        sheet_bin.measure (orientation, for_size, out sheet_min, out sheet_nat, null, null);
        dimming.measure (orientation, for_size, out dimming_min, out dimming_nat, null, null);

        if (orientation == HORIZONTAL) {
            min = int.max (dimming_min, sheet_min);
            nat = int.max (dimming_nat, sheet_nat);
        } else {
            min = int.max (dimming_min, sheet_min + TOP_MARGIN);
            nat = int.max (dimming_nat, sheet_nat + TOP_MARGIN);
        }
        min_baseline = nat_baseline = -1;
    }

    protected override void size_allocate (int width, int height, int baseline) {
        if (!sheet_bin.get_child_visible ())
            return;

        dimming.allocate (width, height, baseline, null);

        int sheet_height, min_sheet_height;

        if (preferred_sheet_height < 0) {
            sheet_bin.measure (VERTICAL, -1, out min_sheet_height, out sheet_height, null, null);
        } else {
            sheet_bin.measure (VERTICAL, -1, out min_sheet_height, null, null, null);
            sheet_height = preferred_sheet_height;
        }

        sheet_height = int.max (sheet_height, min_sheet_height);
        sheet_height = int.min (sheet_height, height - TOP_MARGIN);

        int offset_rounded = (int) Math.round (animation.value * sheet_height);

        var t = new Gsk.Transform ();
        t = t.translate ({ 0, height - offset_rounded });

        sheet_height = int.max (sheet_height, offset_rounded);

        sheet_bin.allocate (width, sheet_height, baseline, t);
    }
}
