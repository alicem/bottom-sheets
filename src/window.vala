/* window.vala
 *
 * Copyright 2023 Alex
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

[GtkTemplate (ui = "/org/gnome/Example/window.ui")]
public class Sheets.Window : SheetToplevel {
    [GtkChild]
    private unowned Sheet sheet;

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    [GtkCallback]
    private void open_sheet () {
        sheet.show_sheet = true;
    }

    [GtkCallback]
    private void close_sheet () {
        sheet.show_sheet = false;
    }

    [GtkCallback]
    private void open_dialog () {
        /*
        var sheet = new SheetWindow (this) {
            default_width = 600,
            default_height = 600,
        };

        var header = new Adw.HeaderBar () {
            show_title = false,
        };

        var page = new Adw.StatusPage () {
            title = "Title",
            description = "Description",
            icon_name = "edit-copy-symbolic",
        };

        var view = new Adw.ToolbarView ();
        view.add_top_bar (header);
        view.content = page;

        sheet.content2 = view;

        sheet.present ();*/

        var dialog = new SheetDialog () {
            preferred_width = 600,
            preferred_height = 600,
        };

        var header = new Adw.HeaderBar () {
            show_title = false,
        };

        var page = new Adw.StatusPage () {
            title = "Title",
            description = "Description",
            icon_name = "edit-copy-symbolic",
        };

        var view = new Adw.ToolbarView ();
        view.add_top_bar (header);
        view.content = page;

        dialog.child = view;

        present_dialog (dialog);
    }
}
